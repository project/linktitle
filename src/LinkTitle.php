<?php

namespace Drupal\linktitle;

use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Url;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\RequestOptions;

/**
 * Handles the retrieval an replacement of link titles.
 */
class LinkTitle implements LinkTitleInterface {

  /**
   * The http client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $client;

  /**
   * The logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Constructs a \Drupal\linktitle\LinkTitle object.
   *
   * @param \GuzzleHttp\Client $http_client
   *   The http client.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerFactory
   *   The logger factory.
   */
  public function __construct(Client $http_client, LoggerChannelFactoryInterface $loggerFactory) {
    $this->client = $http_client;
    $this->logger = $loggerFactory->get('linktitle');
  }

  /**
   * {@inheritdoc}
   */
  public function addTitles(string $snippet): string {
    $dom_document = Html::load($snippet);
    foreach ($dom_document->getElementsByTagName('a') as $link) {
      $title = $link->getAttribute('title');
      $href = $link->getAttribute('href');
      if (!empty($title) || empty($href)) {
        continue;
      }

      $url_title = $this->getTitleFromUrl($href);
      if (!empty($url_title)) {
        $link->setAttribute('title', $url_title);
      }
    }

    return $dom_document->saveHTML();
  }

  /**
   * {@inheritdoc}
   *
   * Use a Guzzle stream to prevent the whole page from being read. The PHP
   * stream API supports reading a stream to a given string through the
   * `stream_get_line`, however that isn't implemented in the Guzzle stream.
   * This method reads the stream in manageable chunks until the `</title>` or
   * `<body>` tag is found.
   */
  public function getTitleFromUrl(string $url): string {
    // Add support for internal urls.
    if (!UrlHelper::isExternal($url)) {
      $url = Url::fromUserInput($url, [
        'absolute' => TRUE,
      ])->toString();
    }

    try {
      $response = $this->client->request('GET', $url, [
        RequestOptions::STREAM => TRUE,
        RequestOptions::ALLOW_REDIRECTS => TRUE,
        RequestOptions::CONNECT_TIMEOUT => 5,
      ]);
    }
    catch (GuzzleException $e) {
      $this->logger->notice($e);
      return '';
    }

    $buffer = '';
    $title_start = FALSE;
    $title_end = FALSE;
    while (!$response->getBody()->eof()) {
      $buffer .= $response->getBody()->read(static::SEEK_LENGTH);
      $title_start = strpos($buffer, '<title>');
      $title_end = strpos($buffer, '</title>');
      $body_start = strpos($buffer, '<body>');

      if ($title_end) {
        $response->getBody()->close();
        break;
      }

      if ($body_start) {
        $response->getBody()->close();
        return '';
      }
    }
    // Account for the tags.
    // substr('<title>') === 7.
    if ($title_start && $title_end) {
      return _filter_html_escape(substr($buffer, ($title_start + 7), ($title_end - $title_start - 7)));
    }
    else {
      return '';
    }
  }

}
